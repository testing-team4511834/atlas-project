const moment = require('moment-timezone');

describe('API Tests', () => {
    it('Verify latest data for UC06', () => {
        const apiKey = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJ2elRXRDY1YmI2MW9tRVYwV3hORlZVUnhoX1pxUkJuNURBdWZ1eHFjZHZRIn0.eyJleHAiOjE2OTg4NDUyMjcsImlhdCI6MTY5ODgxMzI0MywiYXV0aF90aW1lIjoxNjk4ODEyODI3LCJqdGkiOiJkYzMyMWRjYy1lZjAxLTRhMTktOTBiMi1hMDNmNzA1ZDQ4MmUiLCJpc3MiOiJodHRwczovL3Nzby1yaHNzby1rZXljbG9hay5hcHBzLm9jcC0wMS50ZGlnaXRhbC12aXZvLmNvbS5ici9hdXRoL3JlYWxtcy90ZWxjby13ZWItcHJvZCIsImF1ZCI6WyJhdXRob3JpemF0aW9uIiwiYWNjb3VudCJdLCJzdWIiOiIzM2ZlOTc4My03ZTgxLTQ2MzctYmM4My0wMTYwNTdiMTNkM2MiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJtYXN0ZXJDbGllbnQiLCJub25jZSI6ImU4OGVjNjI5LTZlMTktNGFhZC04NmZiLWFiMzRiMzhiNmIzNCIsInNlc3Npb25fc3RhdGUiOiI3YTE0ZTU2Yy1hMGY1LTQ3YzQtODlhMC00MTgwMmMwOGU3YjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiKiJdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiIsImRlZmF1bHQtcm9sZXMtdGVsY28td2ViLXByb2QiXX0sInJlc291cmNlX2FjY2VzcyI6eyJhdXRob3JpemF0aW9uIjp7InJvbGVzIjpbIk5PQy1BZG1pbiJdfSwiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgZW1haWwgcHJvZmlsZSIsInNpZCI6IjdhMTRlNTZjLWEwZjUtNDdjNC04OWEwLTQxODAyYzA4ZTdiMSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwibmFtZSI6Ik1lZXQgUGF0ZWwiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJtZWV0LnBhdGVsQGlhdXJvLmNvbSIsImdpdmVuX25hbWUiOiJNZWV0IiwiZmFtaWx5X25hbWUiOiJQYXRlbCIsInRlbmFuaWQiOiJ0ZWxjby13ZWItcHJvZCIsImVtYWlsIjoibWVldC5wYXRlbEBpYXVyby5jb20ifQ.TIUqhyes6T0CZM4WvT-vQt6R0FpyGLQZEwwrvb90r9V3Yyr9TMGzC9DcY3EYxhCm1A89mgKj2nPFjtJOtbXAiq43AnPVVxthyTlVm5rhVXc3_oVoDr1zE2QIJh4kXozJVAvuZR3-Ew1N3TC34SSakihRyEpz36CXpHQS2mzp9q5ojONOORB915YPO7qMb9c1CL0xGauxBgtE37kOgEeKBVIYXCDKrIOeDucCfMw5KeO4AvWxMtNEQTimJFzy7hqBaye00H1UxxmcZSBmhAZv7d9Cm6BjKJGbyjM3g-Wq7Twv9Hl2EST3a8bX3y0Zb3bTt8Zr-6AGnROnUrs1I6RufA'; // Replace with your actual API key

        cy.request({
            method: 'GET',
            url: 'https://telco-nest-report-service-telco-webapplications-prod.apps.ocp-01.tdigital-vivo.com.br/rawReport/queryRawReport/UC6_data?tab=ftth&page=0&size=20', // Replace with your actual API URL
            headers: {
                'Authorization': `Bearer ${apiKey}`
            }
        })
        .its('body')
        .then(apiResponse => {
            // Extract date and time values from the API response
            const apiDateTime = apiResponse.max_date; // Replace with the actual key for date and time in the API response

            // Parse the API date and time using Moment.js
            const apiDateTimeFormatted = moment.tz(apiDateTime, 'BRT'); // Assuming the API response is in BRT timezone

            // Get current date and time in BRT timezone
            const currentTime = moment.tz('BRT');

            // Log the API date and time and current date and time for debugging
            cy.log('API Date & Time:', apiDateTimeFormatted.format('YYYY-MM-DD HH:mm:ss'));
            cy.log('Current Date & Time:', currentTime.format('YYYY-MM-DD HH:mm:ss'));

            // Assert that the date in the API response is equal to the current date
            expect(apiDateTimeFormatted.format('YYYY-MM-DD')).to.equal(currentTime.format('YYYY-MM-DD'));

            // Assert that the time in the API response is equal to the current time
            expect(apiDateTimeFormatted.format('HH:mm:ss')).to.equal(currentTime.format('HH:mm:ss'));
        });
    });
});
