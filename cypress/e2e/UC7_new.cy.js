const { it } = require("mocha")

describe('Use Case 07 Current Alarms Sanity Testing', function () {

  Cypress.on('uncaught:exception', (err, runnable) => {

    return false
  })




  before(function () {

    cy.fixture('Login').then(function (data) {

      this.data = data
    })
  })



  it('Verify Login functionality of the application', function () {

    
 // cy.visit('https://sso-rhsso-keycloak.apps.mvp.telcostackmvp.br.telefonica.com/auth/realms/telco-web-test/protocol/openid-connect/auth?client_id=masterClient&redirect_uri=https%3A%2F%2Ftelco-fe-telco-webapplications-test.apps.mvp.telcostackmvp.br.telefonica.com%2F%23%2Fhome%2Fdashboard%2Fdashboard-landing%2Fdashboard-details&state=598c1232-ec03-4767-889f-0c7af63e8eb0&response_mode=fragment&response_type=code&scope=openid&nonce=8dbc50dc-1df7-496e-a639-dc63546f1c86')
  cy.visit('https://sso-rhsso-keycloak.apps.ocp-01.tdigital-vivo.com.br/auth/realms/telco-web-prod/protocol/openid-connect/auth?client_id=masterClient&redirect_uri=https%3A%2F%2Ftelco-fe-telco-webapplications-prod.apps.ocp-01.tdigital-vivo.com.br%2F&state=9a406831-90b9-491f-ba02-02ecbccefb4e&response_mode=fragment&response_type=code&scope=openid&nonce=df438827-5325-4aca-b79f-06b8a6acce9d')
   cy.get("#username").type(this.data.username) //username
    cy.wait(5000)
    cy.get("#password").type(this.data.password)
    cy.get("#kc-login").click()
    cy.wait(100000)

    //dev
    //cy.visit('https://rhsso-keycloak-rhsso-keycloak-dev.apps.ocp-01.tdigital-vivo.com.br/auth/realms/telefonica-dev/protocol/openid-connect/auth?client_id=user-login&redirect_uri=https%3A%2F%2Ftelco-fe-telco-webapplications-mvp.apps.ocp-01.tdigital-vivo.com.br%2F&state=1d180032-fa16-439f-8b4c-29aacf2418c3&response_mode=fragment&response_type=code&scope=openid&nonce=de570de0-1937-496e-823d-0f722b057a3d')
    //cy.get("#username").type("meet.patel@iauro.com") //username
    //cy.wait(5000)
    //cy.get("#password").type("iauro100")
    //cy.get("#kc-login").click()
    //cy.wait(100000)

    //Click on Login
    // cy.request('http://telco-frontend-telco-webapplications-test.apps.mvp.telcostackmvp.br.telefonica.com/#/home/dashboard/dashboard-home').should((response) => {
    //expect(response.status).to.eq(200)
    //expect(response).to.have.property('headers')
    //expect(response).to.have.property('duration')

  })

  it('Verify the Current Alarms Page', () => {
    
    cy.xpath('//p[text()=" Fault Monitoring "]').click()
    cy.wait(10000)
    //cy.xpath("//p[text()=' Correlated Alarms (UC 01,02,03) ']").click()
    //cy.wait(20000)
    cy.xpath('//p[text()=" Current Alarms (UC07) "]').click()
    cy.wait(20000)
    
  //  cy.xpath('/html/body/app-root/telco-cmp-home/div/div[1]/telco-cmp-sidebar/div/div[2]/telco-tree/div/div[2]/div[2]/p[2]').click()

  })

  it('Veify the tab functionality', () => {

     cy.wait(5000)
    //cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/mat-tab-header/div/div/div/div[1]/div/div/p').click()
    cy.xpath('//div[@class="mat-tab-label-container"]/descendant::p[1]').should('be.visible')
  })

  it('Veify the Quick filter', () => {

    cy.wait(1000)
   cy.xpath('//div[@class="mat-tab-body-wrapper"]/descendant::img[8]').should('be.visible').click()
   cy.xpath('//p[text()=" Quick Filters "]').should('be.visible')
 })

 it('Veify the Advance filter', () => {

  cy.wait(1000)
 cy.xpath('//p[text()=" Advanced Filters "]').should('be.visible').click()
})

it('Veify the Recent filters tab', () => {

  cy.wait(1000)
 cy.xpath('//p[text()=" Recent Filters "]').should('be.visible').click()
 cy.xpath('//div[@class="cdk-overlay-container"]/descendant::mat-icon[2]').click()
})

  it('Verify the data in Current Alarm table', () => {

    cy.wait(10000)
  //  cy.get("#cdk-drop-list-1 > tbody > tr:nth-child(3) > td.mat-cell.cdk-cell.cdk-column-uuid.mat-column-uuid.ng-tns-c123-13.outer-table-column.ng-star-inserted > div > div").should('not.have.text', "No Data")
    cy.xpath("/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/div/mat-tab-group/div/mat-tab-body[1]/div/telco-cmp-fault-monitoring-all-alarm/div/div[1]/div[1]").should('be.visible')
})

  it('Verify the information page', function () {
    cy.wait(20000)
    cy.get("#row-currentAllAlarms-1 > label > span.mat-checkbox-inner-container.mat-checkbox-inner-container-no-side-margin").dblclick();
    cy.wait(20000)
  })



  it('Verify RCA of the current alarm', () => {

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[1]/div[1]/p').should('not.have.text', "N/A ")
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[3]').should('not.have.text', "N/A ")
  })

  it('Verify severity of the current alarm', () => {

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[1]/div[2]').should('not.have.text', "N/A ")
   cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[4]').should('not.have.text', "N/A ")
  })

  it('Verify the ID of current alarm', () => {

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[2]/div[1]/p').should('not.have.text', "N/A ")
   cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[5]').should('not.have.text', "N/A ")
  })

  it('Verify the description of the current alarm', () => {

    cy.wait(5000)
    //cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[3]/p[1]').should('be.visible')
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[7]').should('be.visible')
    cy.wait(5000)
   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[3]/p[2]').should('not.have.text', "N/A ")
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[8]').should('not.have.text', "N/A ")
  })

  it('Verify description copy icon', () => {

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[3]/p[2]/img').click()
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::img[2]').click()
  })

  //it('Verify the current occurance section', () => {
  it('Verify the First occured section', () => {  

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[4]/div[1]/div[1]/div/p[1]').should('be.visible')
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[9]').should('be.visible')
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[10]').should('not.have.text', " N/A ")
   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[4]/div[1]/div[1]/div/p[2]').should('not.have.text', " N/A ")
  })

  it('Verify the last seen section', () => {

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[4]/div[1]/div[2]/div/p[1]').should('be.visible')
   cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[11]').should('be.visible')
   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[4]/div[1]/div[2]/div/p[2]').should('not.have.text', " N/A ")
   cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[12]').should('not.have.text', " N/A ")
  })

  it('Verify the last update section and last updated date of current alarm', () => {

    //cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[4]/div[1]/div[3]/div/p[1]').should('be.visible')
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[13]').should('be.visible')
  //  cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[4]/div[1]/div[3]/div/p[2]').should('not.have.text', " N/A ")
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[14]').should('not.have.text', " N/A ")

    cy.wait(15000)
   // cy
    //  .get("#mat-tab-content-0-0 > div > div > telco-cmp-all-alarms-details > div.alarm-details-wrap > telco-cmp-correlated-alarm-details > div > div:nth-child(2) > div:nth-child(4) > div:nth-child(1) > div:nth-child(3) > div > p.description.font-noto-400.text-size-16.donjon", { timeout: 15000 })
    //  .invoke('text')
    //  .then(dateText => {
    //    const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

     //   const d = new Date()
    //    let month = d.getMonth()
    //    let day = d.getDate()
    //    let year = d.getFullYear()
    //    const today = new Date()
    //    console.log(d)
    //    console.log(today)
    //    cy.wait(5000)
    //    expect(d).to.be.lte(today)

    //  })


  })

  it('Verify the expire time section', () => {

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[4]/div[1]/div[4]/div/p[1]').should('be.visible')
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[15]').should('be.visible')
  //  cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[4]/div[1]/div[4]/div/p[2]').should('not.have.text', " N/A ")
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[16]').should('not.have.text', " N/A ")
})

  it('Verify the Technology section', () => {

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[4]/div[2]/div[1]/div/p[1]').should('be.visible')
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[17]').should('be.visible')
    // cy.wait(5000)

   // cy.xpath('//*[@id="mat-tab-content-0-0"]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[4]/div[2]/div[1]/div/p[2]').should('not.have.text', " N/A ")
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[18]').should('be.visible')
  })

  it('Verify the agent section', () => {

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[4]/div[2]/div[2]/div/p[1]').should('be.visible')
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[19]').should('be.visible')
   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[4]/div[2]/div[2]/div/p[2]').should('not.have.text', "N/A ")
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[20]').should('not.have.text', "N/A ")
  })

  it('Verify the city id section', () => {

   //cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[4]/div[2]/div[3]/div/p[1]').should('be.visible')
   cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[21]').should('be.visible')
  // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[4]/div[2]/div[3]/div/p[2]').should('not.have.text', " N/A ")
   cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[22]').should('be.visible')
  })

  it('Verify manager section', () => {

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[4]/div[2]/div[4]/div/p[1]').should('be.visible')
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[23]').should('be.visible')
   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[4]/div[2]/div[4]/div/p[2]').should('not.have.text', " N/A ")
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[24]').should('not.have.text', " N/A ")
   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div').scrollTo(0, 500)

  })

  it('Verify the identifier of the current alarm', () => {

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[5]/p[1]').should('be.visible')
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[25]').should('be.visible')
    //cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[5]/p[2]').should('not.have.text', "N/A")
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[26]').should('not.have.text', "N/A")
    // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div').scrollTo(0,500)

  })

  it('Verify the summary of the current alarm', () => {

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[6]/p[1]').should('be.visible')
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[27]').should('be.visible')
   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[6]/p[2]').should('not.have.text', "N/A")
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[28]').should('not.have.text', "N/A")
  })

  it('Verify the persistence section', () => {

    //cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[7]/p[1]').should('be.visible')
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[29]').should('be.visible')
    //cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[7]/p[2]').should('not.have.text', "N/A")
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[30]').should('be.visible')
  })

  it('Verify the SA total section', () => {

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[8]/p[1]').should('be.visible')
  // cy.xpath('//p[text()=" SA Total "]').scrollTo(0, 100) 
   cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[31]').should('be.visible')
   //  cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[2]/div[8]/p[2]').should('not.have.text', "N/A")
    cy.xpath('//div[@class="cdk-overlay-pane"]/descendant::p[32]').should('not.have.text', "N/A")
    
  })

  it('Verify the Alert section ', () => {

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[3]/p').should('be.visible')
    cy.xpath('//div[@class="cdk-global-overlay-wrapper"]/descendant::div[58]').scrollIntoView().should('be.visible'); 
  //  cy.xpath('//div[@class="cdk-global-overlay-wrapper"]/descendant::div[58]').should('be.visible')
    cy.wait(5000)
 //   cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div').scrollTo(0, 500)
   // cy.get('#mat-tab-content-0-0 > div > div > telco-cmp-all-alarms-details > div.alarm-details-wrap > telco-cmp-correlated-alarm-details > div > div:nth-child(4) > div:nth-child(1) > p.text-size-18.font-noto-600').scrollIntoView().should('be.visible')

  })

  it('Verify the alert group of the current alarm', () => {

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[3]/div[1]/p[1]').should('be.visible')
    cy.xpath('//div[@class="cdk-global-overlay-wrapper"]/descendant::p[35]').should('be.visible')
   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[3]/div[1]/p[2]').should('not.have.text', "N/A")
    cy.xpath('//div[@class="cdk-global-overlay-wrapper"]/descendant::p[36]').should('not.have.text', "N/A")
  })

  it('Verify the alert key of the current alarm', () => {

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[3]/div[2]/p[1]').should('be.visible')
    cy.xpath('//div[@class="cdk-global-overlay-wrapper"]/descendant::p[45]').should('be.visible')
   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[3]/div[2]/p[2]').should('not.have.text', "N/A")
    cy.xpath('//div[@class="cdk-global-overlay-wrapper"]/descendant::p[46]').should('not.have.text', "N/A")

  //  cy.get('#mat-tab-content-0-0 > div > div > telco-cmp-all-alarms-details > div.alarm-details-wrap > telco-cmp-correlated-alarm-details > div > div:nth-child(4) > div:nth-child(1) > p.text-size-18.font-noto-600').scrollIntoView().should('be.visible')

    
  })

  it('Verify the site details section', () => {

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[4]/div[1]/p[1]').should('be.visible')
    cy.xpath('//div[@class="cdk-global-overlay-wrapper"]/descendant::div[60]').should('be.visible').click()
    cy.wait(5000)
  //  cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div').scrollTo(0, 1000)

  })

  it('Verify the site name of the current alarm', () => {

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[4]/div[2]/div[1]/div/p[1]').should('be.visible')
    cy.xpath('//div[@class="cdk-global-overlay-wrapper"]/descendant::p[36]').should('be.visible')
   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[4]/div[2]/div[1]/div/p[2]').should('not.have.text', "N/A ")
    cy.xpath('//div[@class="cdk-global-overlay-wrapper"]/descendant::p[37]').should('be.visible')
  })

  it('Verify the status of the current alarm', () => {

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[4]/div[2]/div[2]/div/p').should('be.visible')
    cy.xpath('//div[@class="cdk-global-overlay-wrapper"]/descendant::p[38]').should('be.visible')
   // cy.get("div[class='marg-top-16'] div div[class='status-inside text-size-12']").should('not.have.text', " N/A ")
    cy.xpath('//div[@class="cdk-global-overlay-wrapper"]/descendant::div[81]').should('be.visible')
  })

  it('Verify the state of the current alarm', () => {

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[4]/div[2]/div[3]/div/p[1]').should('be.visible')
    cy.xpath('//div[@class="cdk-global-overlay-wrapper"]/descendant::p[39]').should('be.visible')
   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[4]/div[2]/div[3]/div/p[2]').should('not.have.text', "N/A")
    cy.xpath('//div[@class="cdk-global-overlay-wrapper"]/descendant::p[40]').should('be.visible')
  })

  it('Verify the city of the current alarm', () => {

  //  cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[4]/div[2]/div[4]/div/p[1]').should('be.visible')
    cy.xpath('//div[@class="cdk-global-overlay-wrapper"]/descendant::p[41]').should('be.visible')
   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[4]/div[2]/div[4]/div/p[2]').should('not.have.text', "N/A")
    cy.xpath('//div[@class="cdk-global-overlay-wrapper"]/descendant::p[42]').should('be.visible')

  })

  it('Verify the zip code of the current alarm', () => {

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[4]/div[3]/div/div/p[1]').should('be.visible')
    cy.xpath('//div[@class="cdk-global-overlay-wrapper"]/descendant::p[43]').should('be.visible')
    cy.wait(3000)

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[4]/div[3]/div/div/p[2]').should('not.have.text', "N/A ")
   cy.xpath('//div[@class="cdk-global-overlay-wrapper"]/descendant::p[44]').should('be.visible')
  

  it('Verify the Node Details Section', () => {

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[5]/div[1]/p[1]').should('be.visible')
    cy.xpath('/html/body/div[3]/div[2]/div/mat-dialog-container/telco-cmp-all-alarms-details/div[2]/telco-cmp-current-alarm-details-popup/div/div[3]/mat-tab-group/mat-tab-header/div/div/div/div[3]/div').should('be.visible').click()
    // cy.xpath().should('not.have.text' , "N/A")
    
  })

  it('Verify the equipment id  of the current alarm', () => {

   // cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[5]/div[2]/div[1]/div/p[1]').should('be.visible')
    cy.xpath('/html/body/div[3]/div[2]/div/mat-dialog-container/telco-cmp-all-alarms-details/div[2]/telco-cmp-current-alarm-details-popup/div/div[3]/div/div[2]/div[1]/div/p[1]').should('be.visible')
  //  cy.wait(5000)
  //  cy.get("body > app-root:nth-child(1) > telco-cmp-home:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > telco-cmp-fault-monitoring:nth-child(2) > telco-cmp-fault-monitoring-details:nth-child(2) > telco-cmp-all-alarms-home:nth-child(2) > mat-tab-group:nth-child(1) > div:nth-child(2) > mat-tab-body:nth-child(1) > div:nth-child(1) > div:nth-child(1) > telco-cmp-all-alarms-details:nth-child(1) > div:nth-child(2) > telco-cmp-correlated-alarm-details:nth-child(2) > div:nth-child(1) > div:nth-child(5) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > p:nth-child(2)").should('not.have.text', " N/A ")
    cy.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/telco-cmp-all-alarms-details/div[2]/telco-cmp-current-alarm-details-popup/div/div[3]/div/div[2]/div[1]/div/p[2]").should('not.have.text', " N/A ")
  })

  it('Verify the Domain of the current alarm', () => {

    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[5]/div[2]/div[2]/div/p[1]').should('be.visible')
    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[5]/div[2]/div[2]/div/p[2]').should('not.have.text', "N/A ")
  })



  it('Verify the equipement type of the current alarm', () => {


    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[5]/div[2]/div[3]/div/p[1]').should('be.visible')
    cy.wait(5000)

    cy.get("body > app-root:nth-child(1) > telco-cmp-home:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > telco-cmp-fault-monitoring:nth-child(2) > telco-cmp-fault-monitoring-details:nth-child(2) > telco-cmp-all-alarms-home:nth-child(2) > mat-tab-group:nth-child(1) > div:nth-child(2) > mat-tab-body:nth-child(1) > div:nth-child(1) > div:nth-child(1) > telco-cmp-all-alarms-details:nth-child(1) > div:nth-child(2) > telco-cmp-correlated-alarm-details:nth-child(2) > div:nth-child(1) > div:nth-child(5) > div:nth-child(2) > div:nth-child(3) > div:nth-child(1) > p:nth-child(2)").should('not.have.text', " N/A ")
  })

  it('Verify the vendor of the current alarm', () => {

    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[5]/div[2]/div[4]/div/p[1]').should('be.visible')
    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[5]/div[2]/div[4]/div/p[2]').should('not.have.text', "N/A ")
  })

  it('Verify the POD name of the current alarm', () => {

    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[5]/div[3]/div[1]/div/p[1]').should('be.visible')
    cy.wait(5000)
    cy.get("#mat-tab-content-0-0 > div > div > telco-cmp-all-alarms-details > div.alarm-details-wrap > telco-cmp-correlated-alarm-details > div > div:nth-child(5) > div:nth-child(3) > div:nth-child(1) > div > p.description.font-noto-400.text-size-16.donjon").should('not.have.text', "N/A ")
  })

  it('Verify the Node of the current alarm', () => {

    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[5]/div[3]/div[2]/div/p[1]').should('be.visible')
    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[5]/div[3]/div[2]/div/p[2]').should('not.have.text', "N/A ")
  })

  it('Verify the Node Alias of the current alarm', () => {

    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[5]/div[3]/div[3]/div/p[1]').should('be.visible')
    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[5]/div[3]/div[3]/div/p[2]').should('not.have.text', "N/A ")
  })

  it('Verify the NE ID of the current alarm', () => {

    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[5]/div[3]/div[4]/div/p[1]').should('be.visible')
    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[5]/div[3]/div[4]/div/p[2]').should('not.have.text', "N/A ")

    
  })

  it('Verify the Geographical Details section', () => {

    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[6]/p').should('be.visible')
  })

  it('Verify the city of the current alarm', () => {

    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[6]/div/div[1]/div/p[1]').should('be.visible')
    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[6]/div/div[1]/div/p[2]').should('not.have.text', "N/A ")
  })

  it('Verify the latitude section', () => {

    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[6]/div/div[2]/div/p[1]').should('be.visible')
    cy.wait(3000)

    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[6]/div/div[2]/div/p[2]').should('not.have.text', " N/A ")
  })

  it('Verify the longitude section ', () => {

    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[6]/div/div[3]/div/p[1]').should('be.visible')
    cy.wait(5000)

    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[6]/div/div[3]/div/p[2]').should('not.have.text', " N/A ")
  })

  it('Verify the GC/GDC name section', () => {

    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-fault-monitoring/telco-cmp-fault-monitoring-details/telco-cmp-all-alarms-home/mat-tab-group/div/mat-tab-body[1]/div/div/telco-cmp-all-alarms-details/div[2]/telco-cmp-correlated-alarm-details/div/div[6]/div/div[4]/div/p[1]').should('be.visible')
    cy.wait(5000)

    cy.get("#mat-tab-content-0-0 > div > div > telco-cmp-all-alarms-details > div.alarm-details-wrap > telco-cmp-correlated-alarm-details > div > div:nth-child(6) > div > div:nth-child(4) > div > p.description.font-noto-400.text-size-16.donjon").should('not.have.text', " N/A ")
  })

  it('Verify the close icon of fault monitoring menu', () => {

    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[1]/telco-cmp-sidebar/div/div[2]/telco-tree/div/div[2]/div[1]/div[2]/mat-icon').click()
  }) 
}) 
}) 