const { it } = require("mocha")

describe('Dashboard Sanity Testing', function () {

  Cypress.on('uncaught:exception', (err, runnable) => {

    return false
  })




  before(function () {

    cy.fixture('Login').then(function (data) {

      this.data = data
    })
  })



  it('Verify Login functionality of the application', function () {

   // cy.visit('https://sso-rhsso-keycloak.apps.mvp.telcostackmvp.br.telefonica.com/auth/realms/telco-web-test/protocol/openid-connect/auth?client_id=masterClient&redirect_uri=https%3A%2F%2Ftelco-fe-telco-webapplications-test.apps.mvp.telcostackmvp.br.telefonica.com%2F%23%2Fhome%2Fdashboard%2Fdashboard-landing%2Fdashboard-details&state=598c1232-ec03-4767-889f-0c7af63e8eb0&response_mode=fragment&response_type=code&scope=openid&nonce=8dbc50dc-1df7-496e-a639-dc63546f1c86')
   cy.visit('https://sso-rhsso-keycloak.apps.ocp-01.tdigital-vivo.com.br/auth/realms/telco-web-prod/protocol/openid-connect/auth?client_id=masterClient&redirect_uri=https%3A%2F%2Ftelco-fe-telco-webapplications-prod.apps.ocp-01.tdigital-vivo.com.br%2F&state=9a406831-90b9-491f-ba02-02ecbccefb4e&response_mode=fragment&response_type=code&scope=openid&nonce=df438827-5325-4aca-b79f-06b8a6acce9d')
   cy.get("#username").type(this.data.username) //username
    cy.wait(5000)
    cy.get("#password").type(this.data.password)
    cy.get("#kc-login").click()
    cy.wait(90000)
    
    //Click on Login
    // cy.request('http://telco-frontend-telco-webapplications-test.apps.mvp.telcostackmvp.br.telefonica.com/#/home/dashboard/dashboard-home').should((response) => {
    //expect(response.status).to.eq(200)
    //expect(response).to.have.property('headers')
    //expect(response).to.have.property('duration')

  })

  it('Verify the Active Alarms widget is present or not ', () => {
   
   // cy.get("body > app-root:nth-child(1) > telco-cmp-home:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > telco-cmp-dashboard:nth-child(2) > div:nth-child(3) > telco-cmp-dashboard-home:nth-child(2) > div:nth-child(2) > telco-cards:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > p:nth-child(1)").should('be.visible') // Active Alarm widget present
    cy.xpath("//p[text()='Active Alarms']").should('be.visible') // Active Alarm widget present

    cy.get(".dashboard-cards > telco-cards:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > p:nth-child(1)").should('not.have.text', "N/A")


  })

  //it('Verify the Network Failure Alarms widget is present or not ', () => {

    //cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-dashboard/div[2]/telco-cmp-dashboard-home/div[2]/telco-cards[2]/div/div/div[2]/div[2]/div/p').should('not.have.text', "N/A")

 // })


  it('Verify the severity widget', () => {
    
    cy.get(".home-body.router-outlet.overflow-x-hidden").scrollTo(0, 500)
    cy.get("telco-widget-severity[class='ng-star-inserted'] p[class='font-noto-600 text-size-16 ebony']").should('be.visible')

  })

  it('Verify the Network element group widget', () => {

    cy.wait(2000)
    cy.xpath("//p[text()='Network Element Group']").should('be.visible')
    cy.get(".home-body.router-outlet.overflow-x-hidden").scrollTo(0, 500)

  })

  it('Verify the Time Series Widget', () => {

    cy.get(".home-body.router-outlet.overflow-x-hidden").scrollTo(0, 500)
    cy.xpath("//p[text()='Time Series']").should('be.visible')

  })

  it('Verify the UF widget', () => {

    cy.get("div[class='pad-16 border-bottom-ebb ebony'] p[class='font-noto-600 text-size-16']").should('be.visible')
    cy.get(".home-body.router-outlet.overflow-x-hidden").scrollTo(0, 1000)

  })



  it('Verify the Alarm Type widget', () => {

    cy.get("telco-widget-alarm-type[class='ng-star-inserted'] p[class='font-noto-600 text-size-16 ebony']").should('be.visible')
    
  })

  it('Verify the Technology widget', () => {
    
    cy.get(".home-body.router-outlet.overflow-x-hidden").scrollTo(0, 1500)
    cy.xpath("//p[text()='Technology']").should('be.visible')
  })

  it('Verify the Vendor widget', () => {

    cy.xpath("//p[text()='Vendor']").should('be.visible')
  }) 
}) 