const moment = require('moment-timezone');

describe('API Tests', () => {
    it('Verify latest data for UC08', () => {
        const apiKey = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJ2elRXRDY1YmI2MW9tRVYwV3hORlZVUnhoX1pxUkJuNURBdWZ1eHFjZHZRIn0.eyJleHAiOjE2OTkwMTg5NjQsImlhdCI6MTY5ODk4NjU2OCwiYXV0aF90aW1lIjoxNjk4OTg2NTY0LCJqdGkiOiIyMTVjMjRiMi04Njg4LTQ4MGUtOWJkMy02NTk3NTdkMWI3YWEiLCJpc3MiOiJodHRwczovL3Nzby1yaHNzby1rZXljbG9hay5hcHBzLm9jcC0wMS50ZGlnaXRhbC12aXZvLmNvbS5ici9hdXRoL3JlYWxtcy90ZWxjby13ZWItcHJvZCIsImF1ZCI6WyJhdXRob3JpemF0aW9uIiwiYWNjb3VudCJdLCJzdWIiOiIzM2ZlOTc4My03ZTgxLTQ2MzctYmM4My0wMTYwNTdiMTNkM2MiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJtYXN0ZXJDbGllbnQiLCJub25jZSI6ImEzNGE3Mzk1LTJkY2EtNGQ2MS1hYjhjLWEwZGI3ZGQwNWZmNiIsInNlc3Npb25fc3RhdGUiOiIyZjg2YzNlMC05YmY1LTQ3NGUtODRjMi04ZTM2NjU2OTA2OGEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiKiJdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiIsImRlZmF1bHQtcm9sZXMtdGVsY28td2ViLXByb2QiXX0sInJlc291cmNlX2FjY2VzcyI6eyJhdXRob3JpemF0aW9uIjp7InJvbGVzIjpbIk5PQy1BZG1pbiJdfSwiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgZW1haWwgcHJvZmlsZSIsInNpZCI6IjJmODZjM2UwLTliZjUtNDc0ZS04NGMyLThlMzY2NTY5MDY4YSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwibmFtZSI6Ik1lZXQgUGF0ZWwiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJtZWV0LnBhdGVsQGlhdXJvLmNvbSIsImdpdmVuX25hbWUiOiJNZWV0IiwiZmFtaWx5X25hbWUiOiJQYXRlbCIsInRlbmFuaWQiOiJ0ZWxjby13ZWItcHJvZCIsImVtYWlsIjoibWVldC5wYXRlbEBpYXVyby5jb20ifQ.ScThA1o716mtNrE4oJlUIUyCUqDNom4vsutu3Bqmd2jGQXNWomwGHca-N28oJHZLf3UgOJGqO0buKYtYZZtny9iF1KSQK52bJf2ozpva10vQti77vkVfwCmI0pCgf2nH5enXOa0KXNWa7cIaZ0IG-UpbZPEw7II9cmsF8bU6QKKZFSaoFDiZyUpvtQwZJjrTU1L2yxrZTQUzWg0QMlTOeb9fwWoqdzTODEtwPnG3lCrvdm-WwChOWrn32WLgSf9etG-Fres3-83TJacyjCz9BtPPbkvqLzdP8OaRsyb-c2FncAjoWDHC4uLifK__EsIQDGDWUVckr5MkkV9fVFx_Tg'; // Replace with your actual API key

        cy.request({
            method: 'GET',
            url: 'https://telco-nest-report-service-telco-webapplications-prod.apps.ocp-01.tdigital-vivo.com.br/rawReport/queryRawReport/UC_8_SGInfra_Last_Update?tab=batteries', // Replace with your actual API URL
            headers: {
                'Authorization': `Bearer ${apiKey}`
            }
        })
        .its('body')
        .then(apiResponse => {
            // Extract date and time values from the API response
            const apiDateTime = apiResponse.SG_Infra_Last_Update; // Replace with the actual key for date and time in the API response

            // Parse the API date and time using Moment.js
            const apiDateTimeFormatted = moment.tz(apiDateTime, 'BRT'); // Assuming the API response is in BRT timezone

            // Get current date and time in BRT timezone
            const currentTime = moment.tz('BRT');

            // Log the API date and time and current date and time for debugging
            cy.log('API Date & Time:', apiDateTimeFormatted.format('DD-MM-YYYY HH:mm'));
            cy.log('Current Date & Time:', currentTime.format('DD-MM-YYYY HH:mm'));

            // Assert that the date in the API response is equal to the current date
           // expect(apiDateTimeFormatted.format('DD-MM-YYYY')).to.equal(currentTime.format('DD-MM-YYYY'));

            // Assert that the time in the API response is equal to the current time
           // expect(apiDateTimeFormatted.format('HH:mm')).to.equal(currentTime.format('HH:mm'));

           // Assert that the date in the API response is equal to the current date & time
           expect(apiDateTimeFormatted.format('DD-MM-YYYY HH:mm')).to.equal(currentTime.format('DD-MM-YYYY HH:mm'));
        });
    });
});
