const { it } = require("mocha")

describe('Use Case 8 Batteries Sanity Testing', function () {

Cypress.on('uncaught:exception', (err, runnable) => {
     
      return false
    })

before(function(){

  cy.fixture('Login').then(function(data){

     this.data=data
})
})

it('Verify Login functionality of the application',function()

{
cy.visit('https://sso-rhsso-keycloak.apps.ocp-01.tdigital-vivo.com.br/auth/realms/telco-web-prod/protocol/openid-connect/auth?client_id=masterClient&redirect_uri=https%3A%2F%2Ftelco-fe-telco-webapplications-prod.apps.ocp-01.tdigital-vivo.com.br%2F&state=9a406831-90b9-491f-ba02-02ecbccefb4e&response_mode=fragment&response_type=code&scope=openid&nonce=df438827-5325-4aca-b79f-06b8a6acce9d')
 //cy.visit('https://sso-rhsso-keycloak.apps.mvp.telcostackmvp.br.telefonica.com/auth/realms/telco-web-test/protocol/openid-connect/auth?client_id=masterClient&redirect_uri=https%3A%2F%2Ftelco-fe-telco-webapplications-test.apps.mvp.telcostackmvp.br.telefonica.com%2F%23%2Fhome%2Fdashboard%2Fdashboard-landing%2Fdashboard-details%3BtabIndex%3D636d51f2b431e6d61d5c4474&state=a7b9b338-123b-42be-ac5c-a5e8dd25545e&response_mode=fragment&response_type=code&scope=openid&nonce=b1c48a0c-7584-44d9-9039-e9e91cf8e838')
//cy.visit('https://sso-rhsso-keycloak.apps.mvp.telcostackmvp.br.telefonica.com/auth/realms/telefonica-dev/protocol/openid-connect/auth?client_id=user-login&redirect_uri=https%3A%2F%2Ftelco-fe-telco-webapplications-mvp.apps.mvp.telcostackmvp.br.telefonica.com%2F&state=52d7d1a8-95c4-4472-8689-e8ccbfa6e38d&response_mode=fragment&response_type=code&scope=openid&nonce=921e2563-c455-4685-842d-e1880e0ec50b')
cy.wait(3000)
cy.get("#username").type(this.data.username) //username
cy.get("#password").type(this.data.password)
cy.get("#kc-login").click()

cy.wait(90000)

})

it('Verify Batteries Landing Page', function()
{
  
    cy.xpath("//p[normalize-space()='Battery - Sites Autonomy (UC08)']").click()  //Batteries click
    cy.wait(45000)
})



  it('Verify Sites Running With Batteries Card',function()
  {
  cy.xpath("//p[normalize-space()='Sites Running With Batteries']").should('contain','Sites Running With Batteries')
  cy.xpath("/html[1]/body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-dashboard[1]/gridster[1]/gridster-item[1]/telco-widget-card[1]/div[1]/div[1]/div[1]/p[1]").should('not.have.value','N/A')
  })

  it('Verify 3 dot option at Sites Running With Batteries Card',function()
  {
  cy.xpath("//gridster-item[1]//telco-widget-card[1]//div[2]//button[1]//img[1]").click()
  })

  it('Verify Full Screen View Functionalilty at Sites Running With Batteries Card',function()
  {
  cy.xpath("//p[normalize-space()='Full Screen']").click()
  cy.wait(2000)
  cy.xpath("//mat-icon[normalize-space()='close']").click()
  })


  it('Verify Export Functionalilty at Sites Running With Batteries Card',function()
  {
    cy.xpath("//gridster-item[1]//telco-widget-card[1]//div[2]//button[1]//img[1]").click()
    cy.xpath("//p[normalize-space()='Export']").click()
    cy.wait(2000)
    cy.xpath("//mat-icon[normalize-space()='close']").click()
  })


  it('Verify Drill Down View - Dailog Window for Site Running With Batteries Card',function()
  {
  cy.xpath("//gridster-item[1]//telco-widget-card[1]//div[1]//div[1]//div[1]//p[2]").click()
  cy.wait(2000)
  })

   it('Verify Column Site Name at Dailog window-Site Running With Batteries',function()
   {
   cy.xpath("//div[@class='cdk-overlay-container']//th[1]").should('contain','Site Name')
   cy.xpath("//tbody/tr[3]/td[1]/div[1]/div[1]/div[1]").should('not.have.value','N/A')
   })

   it('Verify Column UF at Dailog window -Site Running With Batteries',function()
   {
   cy.xpath("//div[@class='cdk-overlay-container']//th[2]").should('contain','UF')
   //cy.xpath("/html[1]/body[1]/div[20]/div[2]/div[1]/mat-dialog-container[1]/telco-sites-running-with-batteries-dialog[1]/div[1]/div[2]/div[1]/div[1]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[3]/td[2]/div[1]/div[1]/p[1]").should('not.have.value','N/A')

   })

   it('Verify Column Start Time at Dailog window -Site Running With Batteries',function()
   {
   cy.contains('Start Time').scrollIntoView()
   cy.wait(2000)  
   cy.xpath("//label[normalize-space()='Start Time']").should('contain','Start Time')
   //cy.xpath("//td[@role='cell'])[184]").should('not.have.value','N/A')
   })

   it('Verify Column Predicted Autonomy at Dailog window-Site Running With Batteries',function()
   {
   cy.xpath("//div[@class='cdk-overlay-container']//th[4]").should('contain','Predicted Autonomy')
   //cy.xpath("//body[1]/div[20]/div[2]/div[1]/mat-dialog-container[1]/telco-sites-running-with-batteries-dialog[1]/div[1]/div[2]/div[1]/div[1]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[3]/td[4]").should('not.have.value','N/A')
   })

   it('Verify Close Option in Dailog Window-Site Running With Batteries',function()
   {
    cy.xpath("//mat-icon[normalize-space()='close']").click()
   })




  it('Verify Sites with Discharged Batteries Card',function()
  {
    cy.xpath("//p[normalize-space()='Sites with Discharged Batteries']").should('contain','Sites with Discharged Batteries')
    cy.xpath("/html[1]/body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-dashboard[1]/gridster[1]/gridster-item[2]/telco-widget-card[1]/div[1]/div[1]/div[1]/p[1]").should('not.have.value','N/A')
  })



  it('Verify Sites with Discharged Batteries(>30 min) Card',function()
  {
    cy.xpath("//p[normalize-space()='Sites with Discharged Batteries (>30 min)']").should('contain','Sites with Discharged Batteries (>30 min)')
    cy.xpath("//gridster-item[3]//telco-widget-card[1]//div[1]//div[1]//div[1]//p[1]").should('not.have.value','N/A')
  })
  


  it('Verify No.of Sites Down(More Than 30 Min) Chart',function()
  {
  cy.xpath("//gridster-item[3]//telco-widget-card[1]//div[1]//div[1]//div[1]//p[2]").click()
  cy.wait(2000)
  cy.xpath("//mat-icon[normalize-space()='close']").click()
  })



  it('Verify Autonomy-Classification by Sites Widget',function()
  {
  cy.xpath("//p[normalize-space()='Autonomy (Classification by Sites in Hrs)']").should('contain','Autonomy (Classification by Sites in Hrs)')
  })

  it('Verify Battery Life-Classification by Batteries Widget',function()
  {
  cy.xpath("//p[contains(text(),'Battery Life (Classification by Batteries Rank in ')]").should('contain','Battery Life (Classification by Batteries Rank in')
  cy.xpath("//gridster-item[5]//telco-bar-widget[1]//div[1]//div[1]//div[2]//telco-chart[1]//div[1]//div[1]//canvas[1]").should('be.visible')
})
  



  it('Verify Title for All Alarms Per Region Widget',function()
  {
  cy.contains('All Alarms Per Region').scrollIntoView()
  cy.wait(2000)  
  cy.xpath("//p[normalize-space()='All Alarms Per Region']").should('contain','All Alarms Per Region')
  cy.xpath("//div[@fxlayout='column wrap']//telco-choropleth-map//div//div//canvas").should('be.visible')
  })
  



  it('Verify Alarms Classification Widget',function()
  {
  cy.xpath("//p[normalize-space()='Alarms Classification']").should('contain','Alarms Classification')
  cy.xpath("//mat-tab-body/div/telco-cmp-batteries-dashboard/gridster/gridster-item/telco-pie-widget/div/div[@fxlayout='column']/div[@fxlayout='column']/div[2]").should('be.visible')
  })




  it('Verifying Column - Site Name at Sites Table',function()
  {
  cy.contains("CN").scrollIntoView()
  cy.wait(2000)
  cy.xpath("//label[normalize-space()='Site Name']").should('contain','Site Name')
  cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-dashboard[1]/gridster[1]/gridster-item[8]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[3]/td[1]/div[1]/div[1]/p[1]").should('not.have.value','N/A')
  })

 it('Verifying Column - UF at Sites Table',function()
  {
    cy.xpath("/html[1]/body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-dashboard[1]/gridster[1]/gridster-item[8]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/thead[1]/tr[1]/th[2]/div[1]/div[1]/div[1]/div[1]/label[1]").should('contain','UF')
    cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-dashboard[1]/gridster[1]/gridster-item[8]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[3]/td[2]/div[1]/div[1]/p[1]").should('not.have.value','N/A')
  })

  it('Verifying Column - CN at Sites Table',function()
  {
    cy.xpath("/html[1]/body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-dashboard[1]/gridster[1]/gridster-item[8]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/thead[1]/tr[1]/th[3]").should('contain','CN')
    cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-dashboard[1]/gridster[1]/gridster-item[8]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[3]/td[3]/div[1]/div[1]/p[1]").should('not.have.value','N/A')
  })

  it('Verifying Column - City at Sites Table',function()
  {
    cy.xpath("/html[1]/body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-dashboard[1]/gridster[1]/gridster-item[8]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/thead[1]/tr[1]/th[4]/div[1]/div[1]/div[1]/div[1]").should('contain','City')
    cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-dashboard[1]/gridster[1]/gridster-item[8]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[3]/td[4]/div[1]/div[1]/p[1]").should('not.have.value','N/A')
  })

  it('Verifying Column - No.of Alarms at Sites Table',function()
  {
    cy.contains("No of Alarms").scrollIntoView()
    cy.wait(2000) 
    cy.xpath("//label[normalize-space()='No of Alarms']").should('contain','No of Alarms')
    cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-dashboard[1]/gridster[1]/gridster-item[8]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[3]/td[5]/div[1]/div[1]/p[1]").should('not.have.value','N/A')
  })

  it('Verifying Column - Total Failure Time at Sites Table',function()
  {
    cy.xpath("//label[normalize-space()='Total Failure Time']").should('contain','Total Failure Time')
    cy.xpath("//tbody/tr[3]/td[6]/div[1]/div[1]/p[1]").should('not.have.value','N/A')
  })

  it('Verifying Column - Repair Time Average at Sites Table',function()
  {
    cy.contains("Repair Time Average").scrollIntoView()
   cy.wait(2000)
    cy.xpath("//label[normalize-space()='Repair Time Average']").should('contain','Repair Time Average')
    cy.xpath("//tbody/tr[3]/td[7]/div[1]/div[1]/p[1]").should('not.have.value','N/A')
  })

  it('Verifying Column Title - Predicted Autonomy at Sites Table',function()
  {
    cy.xpath("//label[normalize-space()='Predicted Autonomy']").should('contain','Predicted Autonomy')
    cy.xpath("//tbody/tr[3]/td[8]").should('not.have.value','N/A')
  })

  it('Verifying Column Title - Down Time at Sites Table',function()
  {
    cy.xpath("//label[normalize-space()='Down Time (hr)']").should('contain','Down Time (hr)')
    cy.xpath("//tbody/tr[3]/td[9]/div[1]/div[1]/p[1]").should('not.have.value','N/A')
  })






  it('Verifying Column - Site at Alarm Count Table',function()
  {
    cy.contains("Alarm Count").scrollIntoView()
    cy.wait(2000)
    cy.xpath("//label[normalize-space()='Site']").should('contain','Site')
    cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-dashboard[1]/gridster[1]/gridster-item[9]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[3]/td[1]/div[1]/div[1]/p[1]").should('not.have.value','N/A')
  })

  it('Verifying Column - UF at Alarm Count Table',function()
  {
    cy.xpath("/html[1]/body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-dashboard[1]/gridster[1]/gridster-item[9]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/thead[1]/tr[1]/th[2]").should('contain','UF')
    cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-dashboard[1]/gridster[1]/gridster-item[9]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[3]/td[2]/div[1]/div[1]/p[1]").should('not.have.value','N/A')
  })

  it('Verifying Column - Battery In Discharge at Alarm Count Table',function()
  {
    cy.xpath("//label[normalize-space()='Battery In Discharge']").should('contain','Battery In Discharge')
    cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-dashboard[1]/gridster[1]/gridster-item[9]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[3]/td[3]/div[1]/div[1]/p[1]").should('not.have.value','N/A')
  })

  it('Verifying Column - Site down at Alarm Count Table',function()
  {
    cy.xpath("//label[normalize-space()='Site Down']").should('contain','Site Down')
    cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-dashboard[1]/gridster[1]/gridster-item[9]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[3]/td[4]/div[1]/div[1]/p[1]").should('not.have.value','N/A')
  })

  it('Verifying Column - AC failure at Alarm Count Table',function()
  {
    cy.xpath("//label[normalize-space()='AC Failure']").should('contain','AC Failure')
    cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-dashboard[1]/gridster[1]/gridster-item[9]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[3]/td[5]/div[1]/div[1]/p[1]").should('not.have.value','N/A')
  })

  it('Veriy Site Details Page',function()
  {
   cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-dashboard[1]/gridster[1]/gridster-item[8]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[3]/td[1]").click()
   cy.wait(40000)
  })


   it('Verify Estimated Battery Autonomy Widget',function()
  {
   cy.xpath("//p[normalize-space()='Estimated Battery Autonomy (Hr)']").should('contain','Estimated Battery Autonomy (Hr)')
   cy.xpath("//mat-tab-body/div/telco-cmp-batteries-site-details/gridster/gridster-item/telco-battery-widget/div/div[@fxlayout='column']/div[@fxlayout='column']/div/div[@fxlayout='column']/div[1]").should('be.visible')
  })


  it('Verify DEC Tracks Widget',function()
  {
   cy.xpath("//p[normalize-space()='DEC Tracks']").should('contain','DEC Tracks')
   //cy.xpath("//div[@fxlayoutalign='center start']//div//telco-chart//div//div//canvas").should('be.visible')
  })

  it('Verify Last 1 Year Option in Dropdown for DEC Tracks Widget',function()
  {
   cy.xpath("//div[@fxlayoutalign='center start']//div//div[@fxlayout='row']//telco-list//ng-select//div//div//div[@role='combobox']").click()
   cy.wait(1000)
  // cy.xpath("//span[normalize-space()='Last 1 Year']").click()
   cy.xpath('//ng-dropdown-panel[@aria-label="Options list"]/descendant::div[15]').click()
   cy.wait(3000)
   cy.xpath("//div[@fxlayoutalign='center start']//div//telco-chart//div//div//canvas").should('be.visible')
  })
  

  it('Verify Consumption Voltage Chart',function()
  {
   cy.xpath("//p[normalize-space()='Consumption Voltage']").should('contain','Consumption Voltage')
  })

  


  it('Verify Batteries at Site Table',function()
  {
    cy.contains("Batteries at Site").scrollIntoView()
    cy.wait(2000)
   cy.xpath("//p[normalize-space()='Batteries at Site']").should('contain','Batteries at Site')
  })

  it('Verify Expand Option at Batteries at Site Table',function()
  {
  // cy.xpath("/html[1]/body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-site-details[1]/gridster[1]/gridster-item[4]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[3]/td[1]/div[1]/div[1]/mat-icon[1]").click()
   cy.xpath('//div[@class="home-container"]/descendant::mat-icon[21]').click()
   cy.wait(1000)
  })

  it('Verify Column FCC - Batteries at Site Table',function()
  {
   cy.xpath("//label[normalize-space()='FCC']").should('contain','FCC')
  // cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-site-details[1]/gridster[1]/gridster-item[4]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[3]/td[2]").should('not.have.value','N/A')
   cy.xpath('//div[@class="home-container"]/descendant::p[17]').should('not.have.value','N/A')
  })

  it('Verify Column K-Factor - Batteries at Site Table',function()
  {
   cy.xpath("//label[normalize-space()='K Factor']").should('contain','K Factor')
  // cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-site-details[1]/gridster[1]/gridster-item[4]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[3]/td[3]").should('not.have.value','N/A')
   cy.xpath('//div[@class="home-container"]/descendant::p[18]').should('not.have.value','N/A')
  })

  it('Verify Column Batteries - Batteries at Site Table',function()
  {
   cy.xpath("//label[normalize-space()='Batteries']").should('contain','Batteries')
  // cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-site-details[1]/gridster[1]/gridster-item[4]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[4]").should('not.have.value','N/A')
   cy.xpath('//div[@class="home-container"]/descendant::p[39]').should('not.have.value','N/A')
  })

  it('Verify Column Battery Life - Batteries at Site Table',function()
  {
   cy.xpath("//label[normalize-space()='Battery Life']").should('contain','Battery Life')
  // cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-site-details[1]/gridster[1]/gridster-item[4]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[5]").should('not.have.value','N/A')
  cy.xpath('//div[@class="home-container"]/descendant::p[40]').should('not.have.value','N/A')
  })

  it('Verify Column Bank Number - Batteries at Site Table',function()
  {
   cy.xpath("//label[normalize-space()='Bank Number']").should('contain','Bank Number')
  // cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-site-details[1]/gridster[1]/gridster-item[4]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[6]").should('not.have.value','N/A')
   cy.xpath('//div[@class="home-container"]/descendant::p[41]').should('not.have.value','N/A')
  })

  it('Verify Column Manufacturer - Batteries at Site Table',function()
  {
    cy.contains("Manufacturer").scrollIntoView()
    cy.wait(1000)
    cy.xpath("//label[normalize-space()='Manufacturer']").should('contain','Manufacturer')
  //  cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-site-details[1]/gridster[1]/gridster-item[4]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[7]").should('not.have.value','N/A')
    cy.xpath('//div[@class="home-container"]/descendant::p[42]').should('not.have.value','N/A')
  })

  it('Verify Column Model - Batteries at Site Table',function()
  {
   cy.xpath("//label[normalize-space()='Model']").should('contain','Model')
  // cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-site-details[1]/gridster[1]/gridster-item[4]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[8]").should('not.have.value','N/A')
   cy.xpath('//div[@class="home-container"]/descendant::p[43]').should('not.have.value','N/A')
  })

  it('Verify Column Bank Capacity - Batteries at Site Table',function()
  {
    cy.contains("Bank Capacity").scrollIntoView()
    cy.wait(1000)
   cy.xpath("//label[normalize-space()='Bank Capacity']").should('contain','Bank Capacity')
  // cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-site-details[1]/gridster[1]/gridster-item[4]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[9]").should('not.have.value','N/A')
   cy.xpath('//div[@class="home-container"]/descendant::p[44]').should('not.have.value','N/A')
  })

  it('Verify Column Battery Type - Batteries at Site Table',function()
  {
   cy.xpath("//label[normalize-space()='Battery Type']").should('contain','Battery Type')
  // cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-site-details[1]/gridster[1]/gridster-item[4]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[10]").should('not.have.value','N/A')
   cy.xpath('//div[@class="home-container"]/descendant::p[45]').should('not.have.value','N/A')
  })

  it('Verify Column Element Voltage - Batteries at Site Table',function()
  {
    cy.contains("Element Voltage").scrollIntoView()
    cy.wait(1000)
   cy.xpath("//label[normalize-space()='Element Voltage']").should('contain','Element Voltage')
  // cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-site-details[1]/gridster[1]/gridster-item[4]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[11]").should('not.have.value','N/A')
   cy.xpath('//div[@class="home-container"]/descendant::p[46]').should('not.have.value','N/A')
  })

  it('Verify Column Equipment Type - Batteries at Site Table',function()
  {
   cy.xpath("//label[normalize-space()='Equipment Type']").should('contain','Equipment Type')
  // cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-site-details[1]/gridster[1]/gridster-item[4]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[12]").should('not.have.value','N/A')
   cy.xpath('//div[@class="home-container"]/descendant::p[47]').should('not.have.value','N/A')
  })

  it('Verify Column Company Level - Batteries at Site Table',function()
  {
    cy.contains("Company Level").scrollIntoView()
    cy.wait(1000)
   cy.xpath("//label[normalize-space()='Company Level']").should('contain','Company Level')
  // cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-site-details[1]/gridster[1]/gridster-item[4]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[13]").should('not.have.value','N/A')
   cy.xpath('//div[@class="home-container"]/descendant::p[48]').should('not.have.value','N/A')
  })

  it('Verify Column Floor - Batteries at Site Table',function()
  {
   cy.xpath("//label[normalize-space()='Floor']").should('contain','Floor')
  // cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-site-details[1]/gridster[1]/gridster-item[4]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[14]").should('not.have.value','N/A')
   cy.xpath('//div[@class="home-container"]/descendant::p[49]').should('not.have.value','N/A')
  })

  it('Verify Column No of Element - Batteries at Site Table',function()
  {
    cy.contains("No of Element").scrollIntoView()
    cy.wait(1000)
   cy.xpath("//label[normalize-space()='No of Element']").should('contain','No of Element')
  // cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-site-details[1]/gridster[1]/gridster-item[4]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[15]").should('not.have.value','N/A')
   cy.xpath('//div[@class="home-container"]/descendant::p[50]').should('not.have.value','N/A')
  })

  it('Verify Column Terminal Position - Batteries at Site Table',function()
  {
   cy.xpath("//label[normalize-space()='Terminal Position']").should('contain','Terminal Position')
  // cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-site-details[1]/gridster[1]/gridster-item[4]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[16]").should('not.have.value','N/A')
   cy.xpath('//div[@class="home-container"]/descendant::p[51]').should('not.have.value','N/A')
  })

  it('Verify Column Date of Manufacturer - Batteries at Site Table',function()
  {
    cy.contains("Date of Manufacturer").scrollIntoView()
    cy.wait(1000)
   cy.xpath("//label[normalize-space()='Date of Manufacturer']").should('contain','Date of Manufacturer')
  // cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-site-details[1]/gridster[1]/gridster-item[4]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[17]").should('not.have.value','N/A')
   cy.xpath('//div[@class="home-container"]/descendant::p[52]').should('not.have.value','N/A')
  })

  it('Verify Column Date of Installation - Batteries at Site Table',function()
  {
   cy.xpath("//label[normalize-space()='Date of Installation']").should('contain','Date of Installation')
  // cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-site-details[1]/gridster[1]/gridster-item[4]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[18]").should('not.have.value','N/A')
   cy.xpath('//div[@class="home-container"]/descendant::p[53]').should('not.have.value','N/A')
  })

  it('Verify Column Last Modified - Batteries at Site Table',function()
  {
    cy.contains("Last Modified").scrollIntoView()
    cy.wait(1000)
   cy.xpath("//label[normalize-space()='Last Modified']").should('contain','Last Modified')
 //  cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-site-details[1]/gridster[1]/gridster-item[4]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[4]/td[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[19]").should('not.have.value','N/A')
   cy.xpath('//div[@class="home-container"]/descendant::p[54]').should('not.have.value','N/A')
  })

  it('Verify Column Grouped Battery - Batteries at Site Table',function()
  {
   cy.xpath("//label[normalize-space()='Grouped Battery']").should('contain','Grouped Battery')
 //  cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-site-details[1]/gridster[1]/gridster-item[4]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[3]/td[20]").should('not.have.value','N/A')
   cy.xpath('//div[@class="home-container"]/descendant::p[35]').should('not.have.value','N/A')
  })

  it('Verify Column SG Infra Consumption - Batteries at Site Table',function()
  {
   cy.xpath("//label[normalize-space()='SG Infra Consumption']").should('contain','SG Infra Consumption')
  // cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-batteries[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-batteries-site-details[1]/gridster[1]/gridster-item[4]/telco-widget-table[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table[1]/div[1]/table[1]/tbody[1]/tr[3]/td[21]").should('not.have.value','N/A')
   cy.xpath('//div[@class="home-container"]/descendant::p[36]').should('not.have.value','N/A')
  })



  it('Verify All columns at Current Alarms Section',function()
  {
    cy.contains("Current").scrollIntoView()
    cy.wait(2000)
   cy.xpath("//label[normalize-space()='Type']").should('contain','Type')
   cy.xpath("//label[normalize-space()='Start Time']").should('contain','Start Time')
   cy.xpath("//label[normalize-space()='Alarm Source']").should('contain','Alarm Source')
  })


  it('Verify All columns at Historic Alarms Section',function()
  {
  cy.xpath("//div[contains(text(),'Historic')]").click()
  cy.wait(4000)
  cy.xpath("//label[normalize-space()='Type']").should('contain','Type')
  cy.xpath("//label[normalize-space()='Start Time']").should('contain','Start Time')
  cy.xpath("//label[normalize-space()='Alarm Source']").should('contain','Alarm Source')
  cy.xpath("//label[normalize-space()='End Time']").should('contain','End Time')
  })



})