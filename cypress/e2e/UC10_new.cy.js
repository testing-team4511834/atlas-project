const { it } = require("mocha")

describe('Use Case 10 Heat Map Sanity Testing', function () {

Cypress.on('uncaught:exception', (err, runnable) => {
     
      return false
    })

before(function(){

  cy.fixture('Login').then(function(data){

     this.data=data
})
})

it('Verify Login functionality of the application',function()

{
cy.visit('https://sso-rhsso-keycloak.apps.ocp-01.tdigital-vivo.com.br/auth/realms/telco-web-prod/protocol/openid-connect/auth?client_id=masterClient&redirect_uri=https%3A%2F%2Ftelco-fe-telco-webapplications-prod.apps.ocp-01.tdigital-vivo.com.br%2F&state=9a406831-90b9-491f-ba02-02ecbccefb4e&response_mode=fragment&response_type=code&scope=openid&nonce=df438827-5325-4aca-b79f-06b8a6acce9d')
cy.wait(3000)
cy.get("#username").type(this.data.username) //username
cy.get("#password").type(this.data.password)
cy.get("#kc-login").click()

cy.wait(90000)

})

it('Verify Heat Map Landing Page', function()
{
    //cy.xpath('/html/body/app-root/telco-cmp-home/div/div[1]/telco-cmp-sidebar/div/div[2]/telco-tree/div/div[2]/div/div[1]/p').click()  //FM click
    cy.xpath("/html[1]/body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[1]/telco-cmp-sidebar[1]/div[1]/div[3]/telco-tree[1]/div[1]/div[2]/div[1]/div[1]/p[1]").click()
    cy.xpath("/html[1]/body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[1]/telco-cmp-sidebar[1]/div[1]/div[3]/telco-tree[1]/div[1]/div[2]/div[2]/p[4]").click()
    //cy.get('[style="font-size: 14px; width: 100%; font-family: sans-serif;"] > :nth-child(2) > [fxlayout="column"] > :nth-child(4)').click() // Heat Map click
    cy.wait(25000)
})

 it('Verify Title of Heat Map Page', function()
 {
  cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/div/div[1]/p').should('contain','Heat Map')
 })


   it('Verify Title for Cities with Elevated Risk Card', function()
   {
   cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[1]/telco-cmp-card-widget/div[1]/div/p[1]').should('contain','Cities with Elevated Risk')
   })

   it('Verify Value at Cities with Elevated Risk Card', function()
   {
   cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[1]/telco-cmp-card-widget/div[1]/div/p[2]').should('not.have.value','N/A')
   })

  it('Verify 3 dot option : Cities with Elevated Risk Card',function()
  {
  cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[1]/telco-cmp-card-widget/div[2]/button/img').click()
  })
  
  it('Verify Full Screen view Option : Cities with Elevated Risk Card',function()
  {
    cy.get(':nth-child(1) > p.height-100').click() //Full Screen option click
    cy.get('telco-cmp-full-screen-view.ng-star-inserted > :nth-child(1) > .border-bottom-ebb > .font-noto-600').contains('Cities with Elevated Risk')  // Verify Name on card
    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[1]/telco-cmp-card-widget/div[1]/div/p[2]').should('not.have.value','N/A') //Verify Count in full screen view
    //cy.get('.pointer > .mat-icon').click()  // Close optio
    cy.get('.border-bottom-ebb > .pointer > .mat-icon').click()
  })

  it('Verify Export Option: Cities with Elevated Risk card',function()
  {
    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[1]/telco-cmp-card-widget/div[2]/button/img').click()
    cy.xpath("//p[normalize-space()='Export']").click() //export option
    cy.wait(1000)
    cy.xpath("//p[@class='cod-gray font-noto-700 text-size-16']").contains('Export')
    cy.xpath("//mat-icon[normalize-space()='close']").click() // Cancel symbol click
 })



  it('Verify Title for UF with Elevated Risk Card', function()
  {
    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[2]/telco-cmp-card-widget/div[1]/div/p[1]').should('contain','UF with Elevated Risk')
  })


  it('Verify Value at UF with Elevated Risk Card', function()
  {
    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[2]/telco-cmp-card-widget/div[1]/div/p[2]').should('not.have.value','N/A')
  })

  it('Verify 3 dot option : UF with Elevated Risk Card',function()
  {
    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[2]/telco-cmp-card-widget/div[2]/button/img').click()
  })

  it('Verify Full Screen view Option : UF with Elevated Risk Card',function()
  {
    cy.xpath("//p[normalize-space()='Full Screen']").click()  //Full screen option click
    cy.xpath("//p[@class='font-noto-600 text-size-16 ebony']").contains('UF with Elevated Risk') //Verify Name on card
    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[2]/telco-cmp-card-widget/div[1]/div/p[2]').should('not.have.value','N/A') //Verify count in full screen view
    cy.xpath("//mat-icon[normalize-space()='close']").click() //close option
  })

  it('Verify Export Option: UF with Elevated Risk card',function()
  {
    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[2]/telco-cmp-card-widget/div[2]/button/img').click()
    cy.xpath("//p[normalize-space()='Export']").click()
    cy.wait(1000)
    cy.xpath("//p[@class='cod-gray font-noto-700 text-size-16']").contains('Export')
    cy.xpath("//mat-icon[normalize-space()='close']").click() //cancel symbol click
  })


  it('Verify Title for Total Isolated Cities Card', function()
  {
    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[3]/telco-cmp-card-widget/div[1]/div/p[1]').should('contain','Total Isolated Cities')
  })
  
  it('Verify Value at Total Isolated Cities Card', function()
  {
    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[3]/telco-cmp-card-widget/div[1]/div/p[2]').should('not.have.value','N/A')
  })  

  it('Verify 3 dot option : Total Isolated Cities Card',function()
  {
    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[3]/telco-cmp-card-widget/div[2]/button/img').click()
  })

  it('Verify Full Screen view Option : Total Isolated Cities Card',function()
  {
    cy.xpath("//p[normalize-space()='Full Screen']").click()
    cy.xpath("//p[@class='font-noto-600 text-size-16 ebony']").contains('Total Isolated Cities')
    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[3]/telco-cmp-card-widget/div[1]/div/p[2]').should('not.have.value','N/A') //Verify count in full screen view
    cy.xpath("//mat-icon[normalize-space()='close']").click()
  })

  it('Verify Export Option: Total Isolated Cities card',function()
  {
  cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[3]/telco-cmp-card-widget/div[2]/button/img').click()
  cy.get('telco-cmp-custom-list-dialog.ng-star-inserted > [fxlayout="column"] > :nth-child(2) > .ng-star-inserted').click()
  cy.xpath("//mat-icon[normalize-space()='close']").click()
  })



  it('Verify Title for MAP View',function()
  {
    cy.get('telco-cmp-map-widget-2.ng-star-inserted > :nth-child(1) > .height-100.bg-white > .border-ebb > .pad-12-16 > .font-noto-600').should('contain','Map View')
  })

  it('Verify Full Screen View Option for MAP View',function()
  {
    cy.get('.full-screen > .plus-button').click() // Full Screen View option in MAP
    cy.xpath("//p[@class='font-noto-600 text-size-16 ebony']").contains('Map View')
    cy.xpath("//mat-icon[normalize-space()='close']").click()  // full screen window close
  })

  it('Verify Toggle Button',function()
  {
    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[4]/telco-cmp-map-widget-2/div/div/div/div[1]/div/telco-toggle-button/mat-slide-toggle/label/div/div/div[1]').click()
  })

  it('Verify Title Classification of Criticality Table',function()
  {
    cy.xpath("//p[normalize-space()='Classification of Criticality']").should('contain','Classification of Criticality')
  })

  it('Verify UF Column  in Classification of criticality Table',function()
  {
    cy.xpath("//label[normalize-space()='UF']").should('contain','UF')
    cy.xpath('//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-bbip[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-backbone-ip-landing[1]/gridster[1]/gridster-item[5]/telco-cmp-table-pagination[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table-pagination[1]/div[1]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/div[1]/p[1]').should('not.have.value','N/A')
  })

  it('Verify City Name Column in Classification of criticality Table',function()
  {
  cy.xpath("//label[normalize-space()='City Name']").should('contain','City Name')
  cy.xpath("/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[5]/telco-cmp-table-pagination/div/div/div/div[2]/div/telco-table-pagination/div[1]/table/tbody/tr[2]/td[2]/div/div/p").should('not.have.value','N/A')
  })


  it('Verify Network Type Column in Classification of criticality Table',function()
  {
    cy.xpath("//label[normalize-space()='Network Type']").should('contain','Network Type')
    cy.xpath("//tbody/tr[2]/td[3]/div[1]/div[1]/p[1]").should('not.have.value','N/A')
  })



  it('Verify Total Exit Links Column in Classification of criticality Table',function()
  {
    cy.xpath("//label[normalize-space()='Total Exit Links']").should('contain','Total Exit Links')
    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[5]/telco-cmp-table-pagination/div/div/div/div[2]/div/telco-table-pagination/div[1]/table/tbody/tr[3]/td[4]/div/div/p').should('not.have.value','N/A')
  })


  it('Verify Realtime Route Availability Column in Classification of criticality Table',function()
  {
    cy.xpath("//label[normalize-space()='Realtime Route Availability']").should('contain','Realtime Route Availability')
    cy.xpath('//tbody/tr[2]/td[6]/div[1]/div[1]/div[1]/div[1]/p[1]').should('not.have.value','N/A')
  })


  it('Verify Historic Route Availability Column in Classification of criticality Table',function()
  {
    cy.xpath("//label[normalize-space()='Historic Route Availability']").should('contain','Historic Route Availability')
    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[5]/telco-cmp-table-pagination/div/div/div/div[2]/div/telco-table-pagination/div[1]/table/tbody/tr[2]/td[6]/div/div/div/div/p').should('not.have.value','N/A')
  })


  it('Verify Risk Column in Classification of criticality Table',function()
  {
    cy.xpath("//label[normalize-space()='Risk']").should('contain','Risk')
    cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[5]/telco-cmp-table-pagination/div/div/div/div[2]/div/telco-table-pagination/div[1]/table/tbody/tr[2]/td[7]/div/div/div/div').should('not.have.value','N/A')
  })


  it('Verify Total Outflow Capacity Column in Classification of criticality Table',function()
  {
  cy.xpath("//label[normalize-space()='Total Outflow Capacity']").should('contain','Total Outflow Capacity')
  cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[5]/telco-cmp-table-pagination/div/div/div/div[2]/div/telco-table-pagination/div[1]/table/tbody/tr[2]/td[8]/div/div/div/div/p').should('not.have.value','N/A')
  })


  it('Verify UF - Column Search',function()
  {
   cy.xpath("/html[1]/body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-bbip[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-backbone-ip-landing[1]/gridster[1]/gridster-item[5]/telco-cmp-table-pagination[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table-pagination[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]").type('SP').type('{enter}')
   cy.wait(5000)
   cy.xpath("/html[1]/body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-bbip[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-backbone-ip-landing[1]/gridster[1]/gridster-item[5]/telco-cmp-table-pagination[1]/div[1]/div[1]/div[1]/div[2]/div[1]/telco-table-pagination[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]").clear().type('{enter}')
   cy.wait(5000)
 })


  it('Verify 3 dot option for Classification of Criticality Table',function()
  {
    cy.xpath("(//img[@alt='Menu Icon'])[5]").click()
  })

  it('Verify Full Screen View Option for Classification of Criticality Table',function()
    {
      cy.xpath("//p[normalize-space()='Full Screen']").click()
      cy.xpath("//p[@class='font-noto-600 text-size-16 ebony']").contains('Classification of Criticality')  // Verify Title 
      cy.xpath("//mat-icon[normalize-space()='close']").click()  // Close option in full screen
    })



 it('Verify Title for Top 10 Impacted UF Widget',function()
 {
  cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[6]/telco-cmp-bar-widget/div[1]/div/div/div[1]/p').should('contain','Top 10 Impacted UF')
 })

 it('Verify 3 dot option for Top 10 Impacted UF Widget',function()
 {
  cy.xpath("(//img[@alt='Menu Icon'])[6]").click()
 })

 it('Verify Full Screen View Option for Top 10 Impacted UF Widget',function()
 {
  cy.xpath("//p[normalize-space()='Full Screen']").click() //full screen option 
  cy.xpath("//p[@class='font-noto-600 text-size-16 ebony']").contains('Top 10 Impacted UF') //verify title in full screen
  cy.xpath("//mat-icon[normalize-space()='close']").click() 
 })

 it('Verify Top 10 Impacted UF Widget Data',function()
 {
  cy.xpath('//gridster-item[6]//telco-cmp-bar-widget[1]//div[1]//div[1]//div[1]//div[2]//telco-chart[1]//div[1]//div[1]//canvas[1]').should('not.have.value','No Data Available')
 })


 it('Verify Title for Top 10 Impacted Cities Widget',function()
 {
  cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[7]/telco-cmp-bar-widget/div[1]/div/div/div[1]/p').should('contain','Top 10 Impacted Cities') 
})
 
it('Verify 3 dot option for Top 10 Impacted Cities Widget',function()
{
  cy.xpath("//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-bbip[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-backbone-ip-landing[1]/gridster[1]/gridster-item[7]/telco-cmp-bar-widget[1]/div[2]/button[1]/img[1]").click()
})

it('Verify Full Screen View Option for Top 10 Impacted Cities Widget',function()
 {
  cy.xpath("//p[normalize-space()='Full Screen']").click() //full screen option
  cy.xpath("//p[@class='font-noto-600 text-size-16 ebony']").contains('Top 10 Impacted Cities') // verify title in full screen
  cy.xpath("//mat-icon[normalize-space()='close']").click()
 })


 it('Verify Top 10 Impacted Cities Widget Data',function()
 {
  cy.xpath('//body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-bbip[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-backbone-ip-landing[1]/gridster[1]/gridster-item[7]/telco-cmp-bar-widget[1]/div[1]/div[1]/div[1]/div[2]/telco-chart[1]/div[1]/div[1]').should('not.have.value','No Data Available')
 })


 it('Verify Title for Route Avaialbility Score Widget',function()
 {
  cy.xpath("//p[normalize-space()='Route Availability Score']").should('contain','Route Availability Score')
})

it('Verify 3 dot option for Route Avaialbility Score Widget',function()
{
  cy.xpath("(//img[@alt='Menu Icon'])[8]").click()
})

it('Verify Full Screen View Option for Route Avaialbility Score Widget',function()
 {
  cy.xpath("(//p[normalize-space()='Full Screen'])[1]").click() //full screen option
  cy.xpath("//p[@class='font-noto-600 text-size-16 ebony']").contains('Route Availability Score') //verify title in full screen
  cy.xpath("//mat-icon[normalize-space()='close']").click()
 })

 it('Verify Route Avaialbility Score Widget Data',function()
 {
  cy.xpath('//gridster-item[6]//telco-cmp-bar-widget[1]//div[1]//div[1]//div[1]//div[2]//telco-chart[1]//div[1]//div[1]//canvas[1]').should('not.have.value','No Data Available')
})

 it('Verify Filter popup window',function()
 {
  cy.xpath("//img[@alt='filter']").click() 
 })

 it('Verify Title for Risk of Isolation in Filter Window',function()
 {
  cy.xpath("//p[normalize-space()='Risk of Isolation']").should('contain','Risk of Isolation')
 })

it('Verify Title for Route Availability in Filter Window',function()
{
  cy.xpath("//p[normalize-space()='Route Availability']").should('contain','Route Availability')
})

it('Verify Title for UF in Filter Window',function()
{
 cy.xpath("//p[normalize-space()='UF']").should('contain','UF')
})

it('Verify Title for Historic Route Availability in Filter Window',function()
{
  cy.xpath("//p[normalize-space()='Historic Route Availability']").should('contain','Historic Route Availability')
})


it('Verify Filter Chips in Filter Window',function()
{
  cy.xpath("//div[@class='overflow-x-hidden width-100 height-100']//div[1]//div[1]//div[3]//div[1]//telco-button[1]//button[1]").should('contain','All Ok') 
  cy.xpath("(//p[normalize-space()='Low (up to 24%)'])[1]").should('contain',' Low (up to 24%) ')
  cy.xpath("//button[@class='pointer']//p[contains(text(),'Medium (25 to 34%)')]").should('contain',' Medium (25 to 34%) ')
  cy.xpath("//button[@class='pointer']//p[contains(text(),'High (35-49%)')]").should('contain','High (35-49%)')
  cy.xpath("//button[@class='pointer']//p[contains(text(),'Very High (50-99%)')]").should('contain',' Very High (50-99%) ')
  cy.xpath("//button[@class='pointer']//p[contains(text(),'Isolated(100%)')]").should('contain',"Isolated(100%)")

  cy.xpath("//p[normalize-space()='Above 80%']").should('contain',"Above 80%")
  cy.xpath("//p[normalize-space()='Between 50% - 79%']").should('contain'," Between 50% - 79% ")
  cy.xpath("//p[normalize-space()='Below-49%']").should('contain',"Below-49%")
  cy.xpath("//mat-icon[normalize-space()='close']").click()  //filter window close option 
})
 
 it('Verify City Route Dailog Window',function()
 {
  cy.xpath('/html/body/app-root/telco-cmp-home/div/div[2]/div/telco-cmp-bbip/div/mat-tab-group/div/mat-tab-body/div/telco-cmp-backbone-ip-landing/gridster/gridster-item[5]/telco-cmp-table-pagination/div/div/div/div[2]/div/telco-table-pagination/div[1]/table/tbody/tr[2]/td[1]/div/div/p').click()
  cy.wait(5000)
})

it('Verify Column Title Boundary in City Route Table',function()
{
cy.xpath("//label[normalize-space()='Boundary']").should('contain','Boundary')
})



it('Verify Column Title EILD in City Route Table',function()
{
  cy.xpath("//label[normalize-space()='EILD']").should('contain','EILD')
})


it('Verify Column Title Conexao_FINAL in City Route Table',function()
{
  cy.xpath("//label[normalize-space()='Conexao_final']").should('contain','Conexao_final')
})

it('Verify Column Exit Route in City Route Table',function()
{
  cy.xpath("//label[normalize-space()='Exit Route']").should('contain','Exit Route')
})



it('Verify Column Title Realtime Route Availability in City Route Table',function()
{
  cy.xpath("//div[@class='cdk-overlay-container']//th[5]").should('contain','Realtime Route Availability')
})




it('Verify Column Title Historic Route Availability in City Route Table',function()
{
  cy.xpath("//div[@class='cdk-overlay-container']//th[6]").should('contain','Historic Route Availability')
})


it('Verify Column Title Risk in City Route Table',function()
{
  cy.xpath("//div[@class='cdk-overlay-container']//th[7]").should('contain','Risk')
})


it('Verify Column Title Route Capacity in City Route Table',function()
{
  cy.xpath("//label[normalize-space()='Route Capacity']").should('contain','Route Capacity')
})


it('Verify Column Title Realtime Traffic Capacity in City Route Table',function()
{
  cy.xpath("//label[normalize-space()='Realtime Traffic Capacity']").should('contain','Realtime Traffic Capacity')
})


it('Verify Column Title Route Type in City Route Table',function()
{
  cy.xpath("//label[normalize-space()='Route Type']").should('contain','Route Type')
})


it('Verify Column Title Route in City Route Table',function()
{
  cy.xpath("//label[normalize-space()='Route']").should('contain','Route')
})


it('Verify Column Title From Physical Interface in City Route Table',function()
{
  cy.xpath("//label[normalize-space()='From Physical Interface']").should('contain','From Physical Interface')
})

it('Verify Column Title To Physical Interface in City Route Table',function()
{
  cy.xpath("//label[normalize-space()='To Physical Interface']").should('contain','To Physical Interface')
})



it('Verify Column Title Ticket No in City Route Table',function()
{
  cy.xpath("//label[normalize-space()='Ticket No']").should('contain','Ticket No')
})


it('Verify Cancel Option in City Route Dailog Window',function()
{
 cy.xpath("//mat-icon[normalize-space()='close']").click()
})


it('Verify UF-Dropdown Box',function()
{
cy.xpath("//div[@class='ng-select-container ng-has-value']").click()
cy.wait(2000)

})

it('Verify City-to-City Level MAP View',function()
{
  cy.xpath("/html[1]/body[1]/app-root[1]/telco-cmp-home[1]/div[1]/div[2]/div[1]/telco-cmp-bbip[1]/div[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/telco-cmp-backbone-ip-landing[1]/div[1]/div[2]/telco-list-details[1]/div[1]/ng-select[1]/ng-dropdown-panel[1]/div[1]/div[2]/div[2]/div[1]/div[2]").click()
  cy.wait(5000)
})

it('Verify UF-AC Title at City-to-City Level MAP',function()
{
cy.xpath("//a[@class='active-link purple pointer']").should('contain','AC')
})


})