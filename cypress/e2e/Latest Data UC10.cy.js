const moment = require('moment-timezone');

describe('API Tests', () => {
    it('Verify latest data for UC10', () => {
        const apiKey = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJ2elRXRDY1YmI2MW9tRVYwV3hORlZVUnhoX1pxUkJuNURBdWZ1eHFjZHZRIn0.eyJleHAiOjE2OTg2OTI0NzksImlhdCI6MTY5ODY2MzgwNiwiYXV0aF90aW1lIjoxNjk4NjYwMDc5LCJqdGkiOiI2MzAxNmJmZC0zOWRkLTRhNWUtYmQxZC01NWMwZTAwYmJlNGEiLCJpc3MiOiJodHRwczovL3Nzby1yaHNzby1rZXljbG9hay5hcHBzLm9jcC0wMS50ZGlnaXRhbC12aXZvLmNvbS5ici9hdXRoL3JlYWxtcy90ZWxjby13ZWItcHJvZCIsImF1ZCI6WyJhdXRob3JpemF0aW9uIiwiYWNjb3VudCJdLCJzdWIiOiIzM2ZlOTc4My03ZTgxLTQ2MzctYmM4My0wMTYwNTdiMTNkM2MiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJtYXN0ZXJDbGllbnQiLCJub25jZSI6ImQxZTFhZjM2LTdiODQtNGMwMS04NDI2LTJjNTBlNWI5NDc3OSIsInNlc3Npb25fc3RhdGUiOiIwNDA4OTYyYS1mMDMzLTRkNzctYjI3Mi00MGE0YWMwNDE2YWUiLCJhbGxvd2VkLW9yaWdpbnMiOlsiKiJdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiIsImRlZmF1bHQtcm9sZXMtdGVsY28td2ViLXByb2QiXX0sInJlc291cmNlX2FjY2VzcyI6eyJhdXRob3JpemF0aW9uIjp7InJvbGVzIjpbIk5PQy1BZG1pbiJdfSwiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgZW1haWwgcHJvZmlsZSIsInNpZCI6IjA0MDg5NjJhLWYwMzMtNGQ3Ny1iMjcyLTQwYTRhYzA0MTZhZSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwibmFtZSI6Ik1lZXQgUGF0ZWwiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJtZWV0LnBhdGVsQGlhdXJvLmNvbSIsImdpdmVuX25hbWUiOiJNZWV0IiwiZmFtaWx5X25hbWUiOiJQYXRlbCIsInRlbmFuaWQiOiJ0ZWxjby13ZWItcHJvZCIsImVtYWlsIjoibWVldC5wYXRlbEBpYXVyby5jb20ifQ.MWOkP8Ppn2fT4u1w-76aWuQ112YgU514YMysdOWSwBS6R2M5l8DTxbNHICn32iBvJaePlull9DlDVJ0ze1meTMpC_kidsWMH84F6fBzTyRZCk0D9M0ROJJwnpA01JmwHlDyX_B0SzIBHe3_gwi4IXPpOXNxzy3gFs0nX3L5ehVm_uqOSk1tu9qgROs6e-ZuWsWgaLz2r31oS8gtjpw8cKIyPDjgn5g9qrSB5rN9lY63PQ05aMBhoczTnDqAZPJIvqpWcN-38okgD3NWkzS0RStfJHNBm77d_9BM-zrb7GwJIsr7mZWUcsLPdJU5NiDGGPRHOExwhP3v0PEvurUIx5w'; // Replace with your actual API key

        cy.request({
            method: 'GET',
            url: 'https://telco-nest-report-service-telco-webapplications-prod.apps.ocp-01.tdigital-vivo.com.br/rawReport/queryRawReport/UC_10_Route_Availability_Score?tab=bbip', // Replace with your actual API URL
            headers: {
                'Authorization': `Bearer ${apiKey}`
            }
        })
        .its('body')
        .then(apiResponse => {
            // Extract date and time values from the API response
            const apiDateTime = apiResponse.hhmm; // Replace with the actual key for date and time in the API response

            // Parse the API date and time using Moment.js
            const apiDateTimeFormatted = moment.tz(apiDateTime, 'BRT'); // Assuming the API response is in BRT timezone

            // Get current date and time in BRT timezone
            const currentTime = moment.tz('BRT');

            // Log the API date and time and current date and time for debugging
            cy.log('API Date & Time:', apiDateTimeFormatted.format('YYYY-MM-DD HH:mm:ss'));
            cy.log('Current Date & Time:', currentTime.format('YYYY-MM-DD HH:mm:ss'));

            // Assert that the date in the API response is equal to the current date
            expect(apiDateTimeFormatted.format('YYYY-MM-DD HH:mm:ss')).to.equal(currentTime.format('YYYY-MM-DD HH:mm:ss'));

            // Assert that the time in the API response is equal to the current time
            expect(apiDateTimeFormatted.format('HH:mm:ss')).to.equal(currentTime.format('HH:mm:ss'));
        });
    });
});
