//cypress.config.js

const { defineConfig } = require("cypress");


module.exports = defineConfig({

  reporter: 'cypress-mochawesome-reporter',

  e2e: {

    setupNodeEvents(on, config) {

      require('cypress-mochawesome-reporter/plugin')(on);

    },
    "reporter": "../node_modules/mochawesome/src/mochawesome.js",
    "reporterOptions": {
        "overwrite": false,
        "html": false,
        "json": true
    },

    "env": {
      "EXPECTED_DATE": "01-11-2023",
      "EXPECTED_TIME": "HH:mm"
  }

    
  },

});